import { Menu } from "./Menu";
import { useState } from "react";

export const Navigation = ({ logo }) => {

  const [menuOpen, setMenuOpen] = useState(false);
  return (
    <>
      <Menu menuOpen={menuOpen} setMenuOpen={setMenuOpen}/>
      <header>
        <div className="logo">
          <img src={logo} alt="logo" />
          <h1>Travize</h1>
        </div>

        <nav>
          <ul>
            <li className="language">FR</li>
            <li className="menu-button" onClick={() => setMenuOpen(true)}>
              <span></span>
              <span></span>
              <span></span>
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
};
