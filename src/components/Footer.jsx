import facebookIcon from "../assets/images/facebook.svg";
import instagramIcon from "../assets/images/instagram.svg";
import youtubeIcon from "../assets/images/youtube.svg";

export const Footer = ({ logo }) => {
  return (
    <footer>
      <div className="container">
        <div className="column">
          <img src={logo} alt="logo" />
          <p>Voyager, partager, explorer</p>
          <div className="social">
            <a href="#">
              <img src={facebookIcon} alt="page facebook de Travize" />
            </a>
            <a href="#">
              <img src={instagramIcon} alt="page instagram de Travize" />
            </a>
            <a href="#">
              <img src={youtubeIcon} alt="page youtube de Travize" />
            </a>
          </div>
        </div>
        <div className="column">
          <h3>Nos idées de voyages</h3>
          <ul>
            <li>Île de France</li>
            <li>Pays de la loire</li>
            <li>Normandie</li>
            <li>Côte d'azur </li>
          </ul>
        </div>
        <div className="column">
          <h3>À propos</h3>
          <ul>
            <li>Qui sommes-nous ?</li>
            <li>Contactez nous</li>
            <li>Notre engagement</li>
            <li>Assurances</li>
          </ul>
        </div>
        <div className="column">
          <h3>Mentions légales</h3>
          <ul>
            <li>Informations importantes</li>
          </ul>
        </div>
      </div>
      <div className="copyright">
        <img src={logo} alt="logo" />
        <p>Travize, 2023 ©</p>
      </div>
    </footer>
  );
};
