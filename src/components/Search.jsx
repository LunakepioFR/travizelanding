export const Search = () => {
  return (
    <section className="search">
      <div className="container">
        <form>
          <div className="bloc">
            <label htmlFor="start">Départ</label>
            <input
              type="text"
              id="start"
              name="start"
              placeholder="Gare ou ville"
            />
          </div>
          <div className="bloc">
            <label htmlFor="end">Destination</label>
            <input
              type="text"
              id="end"
              name="end"
              placeholder="Gare ou ville"
            />
          </div>
          <div className="bloc">
            <label htmlFor="date_start">Aller</label>
            <input
              type="date"
              id="date_start"
              name="date_start"
              placeholder="Ajouter une date"
            />
          </div>
          <div className="bloc">
            <label htmlFor="date_end">Retour</label>
            <input
              type="date"
              id="date_end"
              name="date_end"
              placeholder="Ajouter une date"
            />
          </div>
          <div className="bloc">
            <label htmlFor="passengers">Voyageurs</label>
            <input
              type="number"
              id="passengers"
              name="passengers"
              placeholder="1 personne"
            />
          </div>
          <div className="bloc">
            <button type="submit">Rechercher</button>
          </div>
        </form>
      </div>
    </section>
  );
};
