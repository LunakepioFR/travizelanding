import heroImage from "../assets/images/heroImage.svg";
import { Fade } from "react-awesome-reveal";
export const Hero = () => {
  return (
    <section className="hero">
      <div className="container">
        <Fade cascade damping={0.1} direction="up">
        <h1>Découvrez la France <span>autrement !</span></h1>
        </Fade>
      </div>
    </section>
  );
};
