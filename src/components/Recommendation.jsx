import { Fade } from "react-awesome-reveal";
import background from "../assets/images/background.png";
import star from "../assets/images/star.svg";
export const Recommendation = () => {
  function Card({ name, description, rating }) {
    return (
      <div className="card">
        <div className="content">
          <div className="image">
            <img src={background} />
          </div>
          <div className="text">
            <div className="line">
              <h4>{name}</h4>
              <div className="rating">
                <img src={star} />&nbsp;
                {rating}
              </div>
            </div>
            <div className="description">{description}</div>
          </div>
          <div className="button">Voir le plan</div>
        </div>
      </div>
    );
  }
  return (
    <section className="recommendation">
      <h2>Nos suggestions de voyages</h2>
      <p>
        En seulement quelques étapes simples, vous ferez partie de notre
        communauté dynamique de voyageurs. Rejoignez-nous dès maintenant et
        découvrez de nouvelles aventures passionnantes avec des personnes
        partageant les mêmes intérêts que vous !
      </p>
      <div className="container">
      <Fade cascade damping={0.1} direction="up">
      <Card
          name={"Île-De-France"}
          description={"Monuments historiques"}
          rating={4.2}
        />
        <Card
          name={"Normandie"}
          description={"Destinations naturelles"}
          rating={3.8}
        />
        <Card
          name={"Pays de la Loire"}
          description={"Destinations gastronomiques"}
          rating={4.0}
        />
        <Card
          name={"Alpes-côte-d'azure"}
          description={"Destinations naturelles"}
          rating={4.8}
        />
        </Fade>
      </div>
    </section>
  );
};
