export const JoinUs = () => {
  return (
    <section className="join-us">
      <h2>
        Rejoignez-nous pour une aventure inoubliable en France ! Réservez votre
        voyage dès maintenant.
      </h2>
      <div className="container">
        <div className="button">
          <p>En savoir plus</p>
        </div>
        <div className="button">
          <p>Commencez</p>
        </div>
      </div>
    </section>
  );
};
