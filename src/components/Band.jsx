export const Band = () => {
  return (
    <section className="band">
      <h2>Où voulez-vous voyager ?</h2>
      <div className="container">
        <div className="bloc">
          <p>Paris</p>
          <svg
            width="13"
            height="21"
            viewBox="0 0 13 21"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M2.025 20.5L0.25 18.725L8.475 10.5L0.25 2.275L2.025 0.5L12.025 10.5L2.025 20.5Z"
              fill="black"
              fill-opacity="0.5"
            />
          </svg>
          <p>Normandie</p>
        </div>
        <div className="bloc">
          <p>Marseille</p>
          <svg
            width="13"
            height="21"
            viewBox="0 0 13 21"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M2.025 20.5L0.25 18.725L8.475 10.5L0.25 2.275L2.025 0.5L12.025 10.5L2.025 20.5Z"
              fill="black"
              fill-opacity="0.5"
            />
          </svg>
          <p>Pays de la loire</p>
        </div>
        <div className="bloc">
          <p>Nantes</p>
          <svg
            width="13"
            height="21"
            viewBox="0 0 13 21"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M2.025 20.5L0.25 18.725L8.475 10.5L0.25 2.275L2.025 0.5L12.025 10.5L2.025 20.5Z"
              fill="black"
              fill-opacity="0.5"
            />
          </svg>

          <p>Alpes-côte-d'azure</p>
        </div>
      </div>
      <div className="see-more">
        <p>Voir plus</p>{" "}
        <svg
          width="9"
          height="15"
          viewBox="0 0 9 15"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M1.68454 14.5833L0.427246 13.326L6.25329 7.49999L0.427246 1.67395L1.68454 0.416656L8.76787 7.49999L1.68454 14.5833Z"
            fill="white"
          />
        </svg>
      </div>
    </section>
  );
};
