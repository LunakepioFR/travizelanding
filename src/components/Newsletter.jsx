export const Newsletter = () => {
  return (
    <section className="newsletter">
      <div className="container">
        <h2>Inscrivez-vous à notre <br /> Newsletter pour ne rien manqué</h2>
        <form>
          <div className="bloc">
            <input type="text" placeholder="Tapez votre adresse e-mail" />
          </div>
          <button type="submit">S'inscrire</button>
        </form>
      </div>
    </section>
  );
};
