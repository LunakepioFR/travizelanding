import { useState } from "react";
import "./assets/styles/globals.scss";
import { Navigation } from "./components/Navigation";
import logo from "./assets/images/logo.svg";
import { Footer } from "./components/Footer";
import { Hero } from "./components/Hero";
import { Search } from "./components/Search";
import { Process } from "./components/Process";
import { Newsletter } from "./components/Newsletter";
import { Recommendation } from "./components/Recommendation";
import { Theme } from "./components/Theme";
import { Band } from "./components/Band";
import { Promess } from "./components/Promess";
import { JoinUs } from "./components/JoinUs";
import { Reviews } from "./components/Reviews";
function App() {
  
  return (
    <div className="App">
      <Navigation logo={logo} />
      <Hero />
      <Search />
      <Process />
      <Newsletter />
      <Recommendation />
      <Theme />
      <Band />
      <Promess />
      <JoinUs />
      <Reviews />
      <Footer logo={logo} />
    </div>
  );
}

export default App;
